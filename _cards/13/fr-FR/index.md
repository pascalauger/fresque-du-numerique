---
title: Fabrication du réseau et des data centers
backDescription: >-
  Extraction, fabrication des composants, assemblage, transport : des industries
  sont nécessaires pour fabriquer les éléments d'infrastructure réseau et les
  data centers que l'on utilise. La fabrication nécessite à la fois de
  l'**énergie** et des **ressources** notamment des métaux.
---

_Source : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_

Impact en terme d'énergie assez faible, et impact en terme de ressource minoritaire = c'est
clairement un sujet secondaire en comparaison de la fabrication du matériel utilisateur.
