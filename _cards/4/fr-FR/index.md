---
title: Infrastructures réseau
backDescription: >-
  Pour fonctionner, internet et les réseaux ont besoin d'infrastructures :
  câbles terrestres et sous-marins, antennes-relais, satellites, routeurs, box
  internet… Ces infrastructures doivent être alimentées en électricité
---

Les réseaux d’accès mobiles représentent le principal poste de la consommation énergétique des
infrastructures réseau (Rapport France Stratégie "[Maîtriser la consommation du numérique : le
progrès technologique n’y suffira pas](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-empreinte-numerique-octobre.pdf)", 2020, p. 59).

On estime à 1,3 milliard le nombre d'équipements réseau, dont plus de 1 milliard de modem ADSL /
fibre (Chiffre Rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/etude-empreinte-environnementale-du-numerique-mondial/)", 2019,
visible dans la version en ligne)
