---
title: Éco-concevoir les services numériques
backDescription: >-
  En plus d’optimiser le code, l’ éco-conception questionne le besoin, évite les
  fonctionnalités inutiles, et réfléchit en amont aux impacts environnementaux
  du service numérique sur tout son cycle de vie. Cela préserve des ressources
  en phase d’utilisation, et évite aussi le renouvellement accéléré du matériel.
---

Autre manière de voir l'éco-conception : c'est concevoir les logiciels de demain pour qu'ils tournent sur les machines d'hier !

Autres sujets : serveur proche des utilisateurs et énergie décarbonée…

À titre d’exemple, la taille moyenne d’une page web a été multipliée par plus de 4 en 10 ans (source : [httparchive.org/reports/page-weight](https://httparchive.org/reports/page-weight?start=earliest&end=latest&view=list)).

Pour aller plus loin sur ce sujet, de la documentation technique existe :

- le référentiel de bonnes pratiques Ecometer : [ecometer.org/rules](http://www.ecometer.org/rules/)
- le “[Référentiel général d'écoconception de services numériques](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/)” (RGESN), de la Mission interministérielle Numérique écoresponsable.
- La page “[Les bonnes pratiques de l'écoconception de service numérique](https://learninglab.gitlabpages.inria.fr/mooc-impacts-num/mooc-impacts-num-ressources/Partie3/FichesConcept/FC3.4.2-bonnespratiques-MoocImpactNum.html)” du MOOC INRIA.
- le livre "Éco-conception web - Les 115 bonnes pratiques", de Frédéric Bordage
  75
- Cette vidéo atelier “[Les premiers gestes vers l’écoconception d’un site web](https://www.youtube.com/watch?v=OrfNJV7GbYQ)”
