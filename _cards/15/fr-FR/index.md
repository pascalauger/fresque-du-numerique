---
title: Extraction et raffinage
backDescription: >-
  Des matières premières sont extraites de la croûte terrestre puis raffinées
  pour obtenir les énergies fossiles et métaux nécessaires au matériel
  numérique. Extraction et raffinage sont des procédés industriels très
  consommateurs d'énergie, de produits chimiques et d'eau douce. Ils sont la
  cause d'importantes pollutions locales et parfois de problèmes sociaux et
  éthiques. De plus, ces ressources sont finies, présentes en quantité limitées.
---

La photo illustre les quantités phénoménales de minerais qu'il est nécessaire d'extraire de la croûte terrestre. C'est une excavatrice à godets, une machine immense de 15 000 tonnes et 225 mètres de long. Ici c'est pour extraire du charbon en Allemagne (mine de Hambach) mais d'autres machines également énormes servent à l'extraction minière en général.

La roue à godets visible au premier plan mesure plus de 20 mètres de diamètre et comporte 20 godets, capables chacun de contenir 15 m3 de charge. Elle peut creuser 240 000 m3 de terre par jour. ([Wikipedia](https://fr.wikipedia.org/wiki/Bagger_293))

Ressource complémentaire sur le sujet :

- Vidéo “[Promesses de dématérialisation et matérialité minérale](https://www.youtube.com/watch?v=QW9udH0vwlE&list=PLWMNQf5APHgJkUw90Fivza5Ac_dBzVgPT)” par Aurore Stephant, Ingénieure géologue minier chez SystExt : présentation très claire sur les filières minières des matières premières minérales et leurs impacts
- Rapport de SystExt “[Controverses minières](https://www.systext.org/node/1785)”, notamment les pages 14 à 30 pour avoir un premier panorama de la réalité de l’activité minière.
