---
title: Effet rebond
backDescription: >-
  Nous optimisons ressources et énergie depuis longtemps, mais les gains
  d'efficacité prévus grâce aux innovations sont compensés par une augmentation
  des usages et une adaptation des comportements : c'est l'effet rebond. La
  technologie seule ne permet pas de consommer moins de ressources.
---

_Sources :_

- _étude UC Louvain “[Moore’s Law and ICT Innovation in the Anthropocene](https://dial.uclouvain.be/pr/boreal/object/boreal%3A243578/datastream/PDF_01/view)”, D. Bol, T. Pirson & R. Dekimpe 2021_
- _rapport de France Stratégie "[Maîtriser la consommation du numérique : le progrès technologique n’y suffira pas](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-empreinte-numerique-octobre.pdf)", 2020, chapitre 3, p.57_

Le progrès technologique permet des réductions de consommation énergétique **unitaires**, mais la flambée de la quantité de matériel et des usages engendrent une augmentation de la consommation énergétique **en absolu**.

Les progrès technologiques sur l’efficacité énergétique n’ont jamais conduit à compenser l’augmentation des usages : l’impact du numérique ne fera qu’augmenter si nous ne nous donnons pas les moyens de le piloter.

Par exemple : au niveau des infrastructures réseau, l’augmentation des débits disponibles, qui reste actuellement le principal objectif du progrès technologique, provoque par effet rebond une explosion du trafic Internet, qui fait ré-augmenter la consommation. Le gain par octet est donc important, mais la flambée du nombre d’octets échangés provoque en absolu une augmentation de la consommation énergétique.
