---
title: Impact social et éthique
backDescription: >-
  Extraction et raffinage de ressources naturelles, fabrication du matériel, et
  traitement des déchets électroniques peuvent être réalisés dans des conditions
  de travail indécentes : salaires insuffisants pour mener une vie décente,
  manque de protections face aux substances nocives pour la santé, travail
  d'enfants, ou encor etravail forcé.
---

Par exemple dans la phase d’extraction : on sait qu’une partie du Cobalt est extraite par des enfants en République Démocratique du Congo.

En ce qui concerne la phase amont du cycle de vie, une initiative établit un classement des entreprises de l'électronique en terme de droits du travail, minerais des conflits et environnement : [voir-et-agir.ch/it-rating/](https://voir-et-agir.ch/it-rating/)

Des initiatives et sources d’informations complémentaires sur ce sujet : [electronicswatch.org](https://electronicswatch.org/fr) et [goodelectronics.org](https://goodelectronics.org)
