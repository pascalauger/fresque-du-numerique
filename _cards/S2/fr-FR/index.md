---
title: Acheter d'occasion / reconditionné
backDescription: >-
  Cela permet d’allonger la durée de vie des équipements. Le matériel d'occasion
  peut être reconditionné par un professionnel et revendu avec une garantie.
---

Le matériel reconditionné par un pro à l’avantage d’être revendu avec une garantie d’au moins 6 mois. Des associations comme Ecodair, des entreprises comme Backmarket ou Recommerce contribuent à faciliter et faire grandir la pratique de l’achat / vente de matériel de seconde main reconditionné. Sans l’aspect reconditionnement, ces achats / ventes peuvent aussi se faire dans ses réseaux personnels ou via des plateformes de vente d’occasion généralistes.
