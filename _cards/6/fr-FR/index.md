---
title: Utiliser un smartphone
backDescription: >-
  Tout commence par l'utilisation d'équipements numériques… Environ 3,5
  milliards de smartphones sont utilisés dans le monde.
---

_Source : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_

Ordinateur et smartphone sont les 2 équipements auxquels le public pense le plus souvent lorsque
nous parlons de numérique, mais loin d'être les seul
