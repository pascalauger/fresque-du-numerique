---
title: Décyclage et autres limites du recyclage
backDescription: >-
  Beaucoup de matériaux ne se recyclent pas : ils se décyclent, c'est-à-dire
  qu’ils perdent en qualité à chaque cycle. Après avoir été "cyclés" quelques
  fois, ils deviennent non recyclables. De plus, tout procédé industriel de
  recyclage doit être alimenté en énergie et en matières premières.
---

On peut employer le terme de décyclage ou de sous-cyclage (downcycling en anglais). Les expressions telles que “recyclable à l’infini” sont souvent très théoriques et trompeuses.

En termes d'empreinte environnementale, la plupart du temps il est plus intéressant de recycler la matière (lorsque cela est techniquement possible) que de l'extraire de la croûte terrestre. Pour de nombreux métaux, l’énergie consommée est de 60 à 98% plus faible lorsque le métal est obtenu par recyclage que lorsqu’il est extrait du milieu naturel (voir les chiffres par métal dans cet [article EcoInfo CNRS](https://ecoinfo.cnrs.fr/2014/09/03/3-le-recyclage-des-metaux/), section “Recyclage et énergie”)

Cette logique n'est toutefois pas automatique, et les procédés industriels de recyclage ne sont dans tous les cas pas neutres en termes d'impact environnemental.
