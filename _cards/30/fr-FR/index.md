---
title: Santé physique
backDescription: >-
  Les pollutions locales des sols, de l'eau, de l'air peuvent avoir d'importants
  impacts sur la santé humaine : cancers, problèmes respiratoires,
  intoxications…
---

En respirant un air pollué, en buvant une eau qui n'est plus potable ou en consommant des aliments contaminés par des substances toxiques, la santé humaine peut être fortement affectée. Cela est particulièrement vrai dans les régions où il n'y a pas de normes sanitaires et environnementales à respecter.
