---
title: Métaux
backDescription: >-
  Le matériel numérique est composé principalement de métaux, il y en a environ
  50 différents dans un équipement : métaux communs : fer, alu, cuivre, etc. ;
  métaux précieux : or, argent, platine, etc. ; métaux rares : cobalt, tantale,
  indium, etc. Ces métaux sont présents en faible concentration dans des
  minerais extraits de la croûte terrestre.
---

_Source : infographie d'Ingénieurs Sans Frontières - SystExt "[Des métaux dans mon smartphone ?](https://www.systext.org/sites/all/animationreveal/mtxsmp/#/)"_

Il y a un siècle, pour obtenir 1 kg de cuivre il fallait extraire 30 à 40 kg de minerais de la croûte terrestre, aujourd'hui c'est plus de 100 kg de minerais qu’il faut extraire (voir schéma ci-dessous, issu du rapport SystExt). Plus le temps est passé, plus les bons filons se sont épuisés, et plus on s’est attaqué à des filons de moins en moins bons.

![](/illustrations/card18_evolutionglobale_teneurs_en_cuivre.png)

La carte est aussi l’occasion de parler des métaux rares : les métaux rares ne sont pas rares au sens premier du terme, ils sont diffus, présents généralement en très faible concentration, et donc difficiles à extraire. Les terres rares sont une sous-catégorie de métaux rares.

Pour obtenir 1 kg de métal rare il faut extraire de l'ordre de plusieurs tonnes à plusieurs dizaines de tonnes de roche de la croûte terrestre (quantité variable selon les métaux, quelques chiffres ici : [www.institutsapiens.fr/terres-rares-payees-cher/](https://www.institutsapiens.fr/terres-rares-payees-cher/)). Or le numérique est très consommateur de métaux rares car ils ont des propriétés très intéressantes pour les objets hi-tech que l’on veut compacts et mobiles.

Ressources complémentaires sur le sujet :

- Rapport de SystExt “[Controverses minières](https://www.systext.org/node/1785)”, notamment page 47 sur l'évolution globale des teneurs en cuivre de 1900 à 2010 : au début du siècle précédent on était plutôt autour des 3% de concentration, aujourd’hui on est à moins de 1%.
- Rapport de France Stratégie "[La consommation de métaux du numérique : un secteur loin d’être dématérialisé](https://www.strategie.gouv.fr/publications/consommation-de-metaux-numerique-un-secteur-loin-detre-dematerialise)", 2020
- Vidéo “[Les Métaux et leur épuisement](https://www.youtube.com/watch?v=XLRUcdMbZ_M)”, par Le Réveilleur
- Conférence d’Olivier Vidal “[Énergie versus matières premières : La transition est-elle réellement possible ?](https://www.youtube.com/watch?v=TxT7HD4rzP4)”
  53
