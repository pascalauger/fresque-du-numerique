---
backDescription: >-
  Internet est un réseau mondial de matériel informatique interconnecté : d'un
  côté les équipements numériques utilisateurs, et de l'autre, infrastructures
  réseaux et data centers. 62% de l'humanité utilise internet, et le volume de
  données échangées augmente de manière exponentielle.
title: Internet et réseaux
---

_Sources :_

- _Recto : données International Energy Agency, "[Global trends in internet traffic, datacentres workloads and data centre energy use, 2010-2020](https://www.iea.org/data-and-statistics/charts/global-trends-in-internet-traffic-data-centres-workloads-and-data-centre-energy-use-2010-2020)", 2021_
- _Verso : rapport de We Are Social et Hootsuite, "[Digital 2022 Global Overview Report](https://datareportal.com/reports/digital-2022-global-overview-report)", 2022_

On voit bien ici la dynamique exponentielle du déploiement du numérique : le trafic internet mondial a été multiplié par 17 en 10 ans (2010-2020). Un chiffre complémentaire utile : au niveau français, ce sont 88% des plus de 12 ans qui utilisent internet(source: Enquête CREDOC "[Baromètre du numérique, 2019](http://j4.cerpeg.fr/images/blog-numerique/documents/barometre-numerique-2019.pdf)", p.9), donc 12% de la population ado et adulte est exclue du numérique.
