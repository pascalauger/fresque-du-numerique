---
title: Émissions de Gaz à Effet de Serre
backDescription: >-
  Via la consommation d'énergies fossiles, le secteur du numérique est
  responsable de 3 à 4% des émissions mondiales de Gaz à Effet de Serre (GES)
  d'origine humaine. Cela contribue au dérèglement climatique.
---

_Sources :_

- _gauche : ARCEP, dossier “[Empreinte environnementale du numérique](https://www.arcep.fr/la-regulation/grands-dossiers-thematiques-transverses/lempreinte-environnementale-du-numerique.html)”, 2022_
- _droite : rapport GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_

3 à 4% est le chiffre mondial, c'est environ du niveau de la flotte mondiale de camions.

- Émissions du secteur du numérique = ~ 2 Gt CO2e / an (= ~3 à 4% de GES)
- Émissions du secteur du transport routier de marchandise (source : [IEA](https://www.iea.org/topics/transport), "Road freight vehicles") = 2,4 Gt CO2 / an (= ~4% de GES)

Par rapport au camembert “énergie”, le camembert GES est déformé vers la fabrication, car l’énergie de fabrication est très carbonée (extraction + pays asiatiques utilisant beaucoup le charbon), alors que l’énergie liée à l’usage est électrique donc en moyenne moins carbonée.

En France, le numérique représente aussi entre 2 et 5% des GES. Nous avons presque 2 fois plus d’équipements qu’en moyenne mondiale, donc le poids de la fabrication est environ le double, en revanche notre électricité est peu carbonée et donc la part liée à l’usage est très faible (_cf._ étude Green IT “[iNUM : impacts environnementaux du numérique en France](https://www.greenit.fr/impacts-environnementaux-du-numerique-en-france/)”, 2021, et [http://www.senat.fr/rap/r19-555/r19-5551.pdf](https://www.senat.fr/rap/r19-555/r19-5551.pdf)).
