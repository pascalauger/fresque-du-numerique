---
title: Accélération des nouveaux usages
backDescription: >-
  Objets connectés, blockchain, robots, intelligence artificielle, 5G, IT for
  green, véhicule autonome, réalité augmentée, cloud gaming, métavers, humain
  augmenté… Utiles ou futiles, de nouveaux usages du numérique émergent et se
  déploient vite. Ils influencent voire accélèrent nombre d'autres secteurs et
  activités humaines. C'est une évolution collective dont un individu a du mal à
  sortir seul.
---

Les produits et services numériques sont conçus et déployés très souvent sans que leur propre impact sur l’environnement ne soit évalué avec une Analyse de Cycle de Vie rigoureuse, y compris du côté des technologies supposées réduire la consommation d’énergie ou l’impact de nos modes de vie.

La rapide croissance de l’adoption de technologies numériques est régulièrement accompagnée de rapports et d’annonces sur les gains par évitements d’émissions de GES grâce au numérique. Par exemple :

“Parmi les hypothèses les plus optimistes [...], les auteurs estiment que l’enseignement à distance va réduire les trajets vers les lieux d’éducation de 30% ; que la consommation d’énergie des ménages va diminuer de 40% par les ménages grâce aux compteurs intelligents, et de 45% dans le bâti commercial ; que la numérisation va réduire de 65% la consommation d’énergie de l’agriculture, l’usage de fertilisants et les émissions liées à la digestion du bétail ; que la numérisation va réduire la production de voiture de 15% ; que la logistique intelligente va réduire le fret routier de 30%, le fret maritime de 20%, le fret ferroviaire de 25%, le fret aérien de 20% ; que la numérisation va réduire la production d’énergie de 20% ; que le télétravail va réduire les trajets domicile-travail de 53%, les voyages d’affaires en voiture de 80% et les voyages d’affaires en avion de 80% ; que l’optimisation des machines va réduire les émissions du secteur industriel de 40%.” (Source et approfondissement : Rapport “[Que peut le numérique pour la transition écologique ?](https://gauthierroussilhe.com/pdf/NTE-Mars2021.pdf)”, Gauthier Roussilhe, 2021.)

Mais ces réductions théoriques grâce au numérique ne sont pas visibles quand on passe à l’échelle ou ne sont pas suffisamment fortes pour compenser la croissance tendancielle. Les gains d’efficacité et d’optimisation permettent généralement d’augmenter la productivité et le flux de matière et d’énergie associé, pas de le stabiliser ou à plus forte raison de le réduire. Ces baisses de GES grâce au numérique sont très souvent théoriques et prennent peu ou pas en compte les effets rebonds directs ou indirects.
