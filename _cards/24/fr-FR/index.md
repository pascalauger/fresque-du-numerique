---
title: Destruction de la biodiversité
backDescription: >-
  Dérèglement climatique et pollutions sont des causes majeures de perte de
  biodiversité, elle-même essentielle à l'agriculture et à la vie humaine. Pour
  approfondir le sujet, nous vous conseillons l'atelier "La Fresque de la
  Biodiversité"
---

_Source : rapport de l'IPBES "[Rapport de l’évaluation mondiale de la biodiversité et des services écosystémiques](https://ipbes.net/sites/default/files/2020-02/ipbes_global_assessment_report_summary_for_policymakers_fr.pdf)", 2019_

Selon l'IPBES, les cinq facteurs responsables de la perte de biodiversité sont, par ordre décroissant : (1) les changements d’usage des terres et de la mer ; (2) l'exploitation directe de certains organismes ; (3) le changement climatique ; (4) la pollution et (5) les espèces exotiques envahissantes.

Toujours selon l'IPBES, à propos des mines : La superficie totale des terres utilisées pour l'exploitation minière est de moins de 1% (donc minime), mais par ses fortes pollutions qu’elle génère, cette industrie a des effets négatifs importants sur la biodiversité, les émissions, la qualité de l'eau et la santé humaine.
