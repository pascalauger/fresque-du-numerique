---
title: Communiquer et partager
backDescription: >-
  Le numérique nous permet de communiquer, de socialiser à distance et de nous
  informer. Il nous permet également de mettre en commun nos connaissances, ou
  encore d’organiser le partage d'activités, de trajets, d’objets...
---

Ces cinq cartes A, B, C, D, E vont ensemble et représentent le "pourquoi" du numérique, sa raison d'être, les besoins auxquels il répond. Nous faisions cela avant l'ère numérique, mais l'outil numérique nous a permis de faire cela avec une ampleur bien plus grande.

Nous vous recommandons de dire d’emblée aux participant·e·s que ces cartes synthétisent les besoins, qu’elles constituent le début de la Fresque, et qu’il ne faudra pas chercher à les séparer (par exemple en formant un cadre autour). Cela évitera des conversations “d’oeuf et de poule”.
