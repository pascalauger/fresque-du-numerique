---
title: Recyclage partiel
backDescription: >-
  Les déchets électroniques collectés peuvent entrer en fillière de recyclage.
  Une faible partie de la matière sera séparée et recyclée, mais la majeure
  partie reste non-recyclable et finit en centre d'enfouissement ou en
  incinération. Le recyclage est donc une solution très partielle, mais il y a
  également d'autres limites au recyclage.
---

_Sources, de gauche à droite :_

- _Au niveau mondial, 17% des DEEE sont collectés en vue d'un recyclage. Source : rapport "[The Global E-waste Monitor 2020](https://www.itu.int/en/ITU-D/Environment/Documents/Toolbox/GEM_2020_def.pdf)", p. 23 (à niveau européen 42,5%, même source, p. 76)_
- _Une fois collecté, on peut estimer qu'environ 75% du matériel est recyclé. Source : ADEME "Équipements électriques et électroniques DEEE, données 2019", p.38 (chiffre France)_
- _Une fois traité en recyclage, une étude de Fairphone montre qu'au mieux 30% de la matière d'un smartphone peut être recyclée._

Une fois que le matériel entre dans la filière de recyclage, l'essentiel de la matière n'est en fait pas recyclée. Une [étude menée par Fairphone](https://www.fairphone.com/fr/2017/03/15/recyclable-fairphone-2/) montre que dans le meilleur des cas 30% de la matière du téléphone peut être recyclée, et une l’étude INSEE “[Impacts environnementaux du numérique](https://www.insee.fr/fr/statistiques/4238589?sommaire=4238635#consulter)” parle de 18% de la matière d'un smartphone recyclée en moyenne.

Bien faire la différence entre les différentes terminologies :

- collecté = entre dans une filière pour être recyclé
- recyclé = la matière séparée et réutilisée pour être remise dans le cycle
- valorisé = incinéré avec récupération de la chaleur de la combustion pour produire de l’énergie.

Des raisons techniques ou économiques limitent le développement du recyclage des déchets électroniques :

- les métaux, présents dans des quantités très faibles, sont le plus souvent utilisés sous la forme d’alliages complexes. Or, si ces alliages permettent d’amplifier les performances et rendent possible la miniaturisation des équipements, ils complexifient énormément le recyclage ;
- l’industrialisation des processus de recyclage dans le numérique est complexifiée par la multiplicité des équipements ;
- les métaux subissent une “dégradation de l’usage” : le métal recyclé peut perdre une partie de ses performances technologiques. (= décyclage) (intro Rapport de France Stratégie, "[La consommation de métaux du numérique : un secteur loin d’être dématérialisé](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-consommation-metaux-du-numerique-juin.pdf)", 2020).

Vous trouverez des infos complémentaires sur le recyclage des plastiques issus du numérique dans cet article de [EcoInfo CNRS](https://ecoinfo.cnrs.fr/2016/05/10/recyclage-des-plastiques/).

Pour illustrer la difficulté à recycler de la matière utilisée en faible quantité et en alliages : ce serait comme vouloir séparer une baguette de pain en farine, eau, sel et levure ! Autres analogies rigolotes : qui essaie de récupérer les tomates une fois que la ratatouille est cuite ? Ou qui peut récupérer la farine quand la tarte aux pommes est cuite ?
