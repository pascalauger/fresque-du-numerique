---
title: Commercer
backDescription: >-
  Le numérique facilite les échanges commerciaux, simplifie l'accès à la
  consommation et augmente les flux de marchandise.
---

Voir [carte A](/card/A).
