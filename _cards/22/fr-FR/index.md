---
title: Exportation illégale & décharge sauvage
backDescription: >-
  Au moins 60% des déchets électroniques sont gérés par des circuits illégaux.
  Ils sont alors souvent retraités dans des conditions humaines et
  environnementales désastreuses pour en extraire quelques éléments de valeur
  avant d'abandonner les restes, souvent toxiques, dans des décharges sauvages.
---

_Source : rapport de l'ONU "[Waste Crimes, Waste Risks: Gaps and Challenges In the Waste Sector](https://www.unenvironment.org/news-and-stories/press-release/illegally-traded-and-dumped-e-waste-worth-19-billion-annually-poses)", 2015_

L’Asie et notamment la Chine ont longtemps été la terre d’accueil privilégiée pour les DEEE. Aujourd’hui, la Chine a fermé ses portes aux DEEE et une bonne partie finit dans d'autres pays d'Asie ou en Afrique de l’Ouest, principalement au Ghana et Nigéria.

Sur la photo c'est Agbogbloshie au Ghana, un des 10 sites les plus pollués de la planète selon une étude, avec notamment du cadmium, plomb, mercure ou encore arsenic dans l’air, la terre et l’eau.

Interpol évalue la valeur des déchets électroniques sur le marché illégal à 500 dollars la tonne (plus d’infos dans cet [article d’ONU](https://europa.eu/capacity4dev/unep/discussions/illegally-traded-and-dumped-e-waste-worth-19-billion-annually-poses-risks-health)), ce qui explique l’existence de ces filières.

Or, cuivre et aluminium représentent les principales sources de valeur des déchets électroniques (voir rapport de France Stratégie, "[La consommation de métaux du numérique : un secteur loin d’être dématérialisé](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-consommation-metaux-du-numerique-juin.pdf)", 2020, p. 38-39
