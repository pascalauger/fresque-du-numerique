---
title: Calculer
backDescription: >-
  Le numérique nous permet de réaliser beaucoup de calculs qui nous seraient
  sinon impossibles, d'une simple requête sur un moteur de recherche aux travaux
  scientifiques dans de nombreux domaines, en passant par le big data.
---

Voir [carte A](/card/A).
