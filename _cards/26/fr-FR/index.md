---
title: 'Pollution des sols, de l''eau, de l''air'
backDescription: >-
  Des matières toxiques sont émises lors de l'extraction et raffinage de
  ressources naturelles, lors de la fabrication du matériel, ainsi que lors du
  traitement de déchets électroniques. Cela génère des pollutions des sols, de
  l'eau et de l'air. Ces pollutions locales détruisent des écosystèmes, nuisent
  à la biodiversité, et affectent la santé humaine.
---

_Source : Pour la partie extraction et raffinage, rapport de France Stratégie, "[La consommation de métaux du numérique : un secteur loin d’être dématérialisé](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-consommation-metaux-du-numerique-juin.pdf)", 2020, p. 27_

Exemples :

- L'extraction et raffinage des terres rares, qui sont une sous-catégorie de métaux rares, est réalisée principalement au nord de la Chine, en Mongolie intérieure. Elle entraîne d'importants rejets de métaux lourds et produits chimiques dans l'environnement.
- Les décharges sauvages, comme celle d’Agbogbloshie, causent de fortes pollutions locales.
