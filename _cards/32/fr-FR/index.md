---
title: Rupture de continuité des services numériques
backDescription: >-
  En cas de pénuries de ressources clés en métaux ou énergie pour fabriquer et
  aleminter le matériel numérique, des ruptures de continuité de services sont
  possibles. Le numérique pourrait alors manquer là où il est utile.
---

Cette carte illustre une des conséquences probables des pénuries de ressources : des équipements numériques de plus en plus difficiles à produire ou à réparer, avec à la clé des ruptures de services de plus en plus fréquentes. Le serpent qui se mord la queue en somme.

Le lien n’est pas fait dans la correction pour ne pas complexifier, mais les événements climatiques extrêmes causés par le dérèglement climatique peuvent aussi causer des ruptures temporaires de continuité de services numériques. Exemples : en cas de tempête, cyclone ou incendie, les télécommunications et autres services numériques peuvent être dégradés ou hors service, et donc manquer notamment pour les secours.
