---
title: Mesurer l'impact environnemental
backDescription: >-
  Mesurer l’ensemble des impacts à toutes les étapes du cycle de vie des
  produits et services, pour mieux cibler ses principaux axes d'amélioration. En
  entreprise notamment, la mesure est une étape essentielle pour un numérique
  plus durable : 1. comprendre, 2. mesurer, 3. éviter, 4. réduire
---

Prendre en compte les risques environnementaux dans la stratégie informatique. Plus cet objectif est énoncé tôt, plus l’organisation sera en position favorable quand l’écosystème se transformera avec :

- Une réglementation plus contraignante
- Des jeunes diplômés soucieux de travailler pour un employeur qui ne compromet pas leur avenir
- Des clients soucieux d’utiliser des services respectueux de l’environnement à un prix optimal.
- Des contraintes sur le prix et l’accès à l’énergie qui auront un impact direct sur le prix de l’informatique
