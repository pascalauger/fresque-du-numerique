---
title: Dé-numériser et être dans la sobriété
backDescription: >-
---

Pour en apprendre plus sur la philosophie Low-tech, RDV par exemple sur [le site de l’association Low Tech Lab](https://lowtechlab.org/fr).

Si l’on repense aux meilleurs moments que l’on passe dans sa semaine ou dans sa vie, ceux-ci sont rarement sur son smartphone ou derrière un écran
