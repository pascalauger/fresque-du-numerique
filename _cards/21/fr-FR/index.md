---
title: Recyclage partiel
backDescription: >-
  Il s'agit de la réduction de la durée de vie et d'utilisation d'un bien par
  des facteurs psychologiques : l'image du produit est rapidement dévalorisée
  auprès de l'utilisateur via des renouvellements fréquents de gamme, du
  marketing intensif, des effets de mode… 88% de Français changent de smartphone
  alors que l'ancien fonctionne encore.
---

_Source : guide de l'ADEME "[La face cachée du numérique](https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-face-cachee-numerique.pdf)", 2019_

On parle d'obsolescence psychologique, d'obsolescence culturelle ou d'obsolescence esthétique.

Pour aller plus loin, voir la section “Obsolescences” dans la partie approfondissement de ce guide
