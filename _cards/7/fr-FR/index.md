---
title: Utiliser un équipement numérique
backDescription: >-
  Tout commence par l'utilisation d'équipements numériques… Au total 34
  milliards d'équipements numériques sont utilisés dans le monde : au-del des
  ordinateurs et smartphones, ce sont aussi des télévisions, tablettes,
  imprimantes, consoles de jeux vidéo, objets connectés, caméras de
  surveillance, écrans publicitaires…
---

_Source : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_

En nombre, plus de la moitié de ces équipements sont des objets connectés. Un habitant d'Europe occidentale possède en moyen 9 équipements électroniques en 2021 (source : [guide ADEME, "La face cachée du numérique](https://www.ademe.fr/sites/default/files/assets/documents/guide-pratique-face-cachee-numerique.pdf)" 2021, p.4)
