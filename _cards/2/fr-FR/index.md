---
title: Électricité consommée à l’utilisation
backDescription: >-
  Quand on les utilise, équipements numériques, infrastructures réseau, et data
  centers consomment de l'électricité. Ce sont les équipements numériques qui
  consomment le plus. La majorité de l'électricité mondiale est produite en
  consommant des énergies fossiles.
---

Sources :

- Recto : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019
- Verso : rapport de International Energy Agency "[Electricity Information: Overview](https://www.iea.org/reports/electricity-information-overview/electricity-production)", section sur la
  production d’électricité, 2021

On ne parle ici que de la **phase d'utilisation**, la phase de fabrication vient plus tard dans l'atelier.
En fin de lot 1, faire un focus sur le camembert de répartition de l’énergie utilisée à l’utilisation : le premier consommateur est l’utilisation des terminaux utilisateur, et le dernier sont les data centers.

L'énergie utilisée dans la phase d'utilisation est quasi-uniquement sous forme d'électricité, ce qui
n'est pas le cas dans la phase de fabrication. En France, cette répartition de la consommation électrique semble conservée (cf. étude Green IT "[iNUM : impacts environnementaux du numérique en France"](https://www.greenit.fr/impacts-environnementaux-du-numerique-en-france/), 2021).

Tout dépend du type d’impact analysé, mais en ordre de grandeur les impacts liés à un usage
particuliers sont plutôt de la moitié à 2/3, et les impacts liés à un usage professionnel sont plutôt de ⅓ à la moitié. Pour aller plus loin, voir les pages 17 et 18 du rapport ARCEP et ADEME "[Evaluation environnementale des équipements et infrastructures numériques en France - Synthèse du 2ème volet de l'étude](https://www.arcep.fr/uploads/tx_gspublication/etude-numerique-environnement-ademe-arcep-volet02-synthese_janv2022.pdf)”, 2022.

Concernant l’évolution dans le temps : la part des data centers a eu plutôt tendance à diminuer sur la décennie 2010, car même si leur nombre augmente, il y a eu des progrès majeurs en termes d’efficience énergétique (plus d’infos sur [cet article de Green IT](https://www.greenit.fr/2020/03/04/data-center-seulement-6-de-hausse-en-8-ans/)). Dans le même temps, nos équipements numériques sont de plus en plus nombreux et leur efficacité ne progresse pas autant (par exemple, la batterie de nos smartphones ne tient pas plus longtemps, alors pourtant qu’elle est de plus en plus grosse).

Plusieurs experts pensent que la dynamique devrait s’inverser, avec dans les années à venir une augmentation de la part des data centers dans la consommation d’énergie, notamment par l’accroissement exponentiel de données traitées et stockées, le développement des serveurs en “edge computing”, et aussi par l’atteinte de limites physiques en terme de gains d’efficience énergétique possibles des data centers (le rendement marginal des travaux d'efficacité énergétique est décroissant).

Avec cette carte, attention à la confusion entre électricité et énergie : l’électricité est un type d’énergie, une énergie secondaire qui a nécessité d’utiliser une énergie primaire pour être produite (principalement gaz et charbon, mais aussi hydroélectricité, nucléaire...). De toute l’énergie consommée par l’humanité, environ 20% passe par la forme d’électricité, la majorité est donc consommée sous d'autres formes (chaudière qui brûle directement du gaz ou du fioul, moteur de la voiture qui brûle directement de l’essence...)
