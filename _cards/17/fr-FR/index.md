---
title: Enfouissement et incinération
backDescription: >-
  Une part des déchets électroniques finit en centre d'enfouissement ou en
  incinérateur. On parle de "valorisation énergétique" pour l'incinération :
  brûler des déchets pour produire de l’énergie. Cela émet des gaz toxiques et
  laisse environ 300 kg de résidus solides et cendres par tonne brûlée. Ces
  déchets enfouis, gaz et résidus peuvent polluer les sols, l'eau et l'air
---

_Source : rapport du Sénat "[Recyclage et valorisation des déchets ménagers](https://www.senat.fr/rap/o98-415/o98-41516.html)", 1999_

- Enfouissement = décharge réglementaire = stockage des déchets = “Centre d’enfouissement
  technique” pour son appellation officielle en France.
- Incinération = “Centre de Valorisation énergétique” pour son appellation officielle en France.

Les résidus solides issus de l’incinération sont nommés mâchefers : ces résidus d'incinération laissés
en fond de four sont constitués dans leur grande majorité des matériaux incombustibles des
déchets (verre, métal...). Les REFIOMs sont quant à eux les résidus d'épuration des fumées
d'incinération des ordures ménagères.

Attention : des participant·e·s peuvent faire le lien entre cette carte et la carte “Émissions de Gaz à
Effet de Serre”, or, même s’il n’existe pas à notre connaissance d’étude permettant de chiffrer
convenablement cela, la contribution aux émissions de GES du CO2 issu de la combustion d’une
partie des déchets électroniques en fin de vie est vraisemblablement très négligeable, donc ce lien
n’est pas correct.
