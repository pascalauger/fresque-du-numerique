---
title: Réduire notre consommation électrique
backDescription: >-
  Réduire le superflu : par exemple une box internet consomme autant qu’un
  réfrigérateur. Privilégier l’électricité bas carbone, mais toute énergie a de
  multiples impacts : CO2, biodiversité, extraction de ressources, pollutions...
  et les énergies renouvelables posent aussi des problèmes. Il n'existe aucune
  énergie "propre". Le plus efficace est donc d’en consommer moins.
---

Une box internet consomme en moyenne 100 kWh par an (puissance de 7 à 17W) c'est l'équivalent d'un réfrigérateur de taille moyenne récent et de classe A.
