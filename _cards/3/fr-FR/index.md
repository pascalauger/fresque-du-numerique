---
title: Utiliser un ordinateur
backDescription: >-
  Tout commence par l'utilisation d'équipements numériques… Environ 1,4
  milliards d'ordinateurs sont utilisés dans le monde
---

_Source : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019._

Ordinateur et smartphone sont les 2 équipements auxquels le public pense le plus souvent lorsque nous parlons de numérique, mais loin d'être les seuls
