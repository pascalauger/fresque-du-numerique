---
title: Pénurie de ressources
backDescription: >-
  Les ressources en énergies fossiles et en métaux sont limitées : plus une
  ressource est extraite, plus elle se raréfie, et plus son extraction se fait
  avec un coût économique, énergétique et environnemental important. Pénuries et
  flambées de prix de ressources critiques sont possibles, ce qui peut provoquer
  des tensions géopolitiques et des ruptures de continuité de services
  numériques.
---

_Sources :_

- _Recto : simplification du graphe issu du livre “[Matières premières et énergie](https://www.istegroup.com/fr/produit/matieres-premieres-et-energie/)”, Olivier Vidal, p.50_
- _Verso : rapport de France Stratégie, "[La consommation de métaux du numérique : un secteur loin d’être dématérialisé](https://www.strategie.gouv.fr/sites/strategie.gouv.fr/files/atoms/files/fs-2020-dt-consommation-metaux-du-numerique-juin.pdf)", p. 11, 2020_

"Notre connaissance des gisements minéraux reste limitée à une partie superficielle de la croûte terrestre. La notion de réserves ne correspond qu’à un horizon technico-économique et temporel des acteurs miniers. Nul ne sait dire avec une certaine fiabilité quelles sont les ressources ultimes dans la partie supérieure de la croûte terrestre. Cependant les nouveaux gisements seront plus difficiles à trouver, nécessiteront plus d’investissement, leur exploitation nécessitera plus d’énergie et ils laisseront plus de résidus pour une même tonne de métal produite." (Rapport ADEME "[L’épuisement des métaux et minéraux : Faut-il s'inquiéter ?](https://www.ademe.fr/sites/default/files/assets/documents/epuisement-metaux-mineraux-fiche-technique.pdf)", 2017)

La perspective pour les décennies à venir n’est pas un scénario “à la Mad Max” avec du jour au lendemain des gisements vides, mais plutôt des gisements de plus en plus difficiles à trouver, difficiles d’accès, et de moins en moins productifs, avec en face des technologies d’extraction qui ne progressent pas assez vite pour suivre le rythme. Ce qui implique des métaux plus chers et plus difficiles à approvisionner... le tout face à une demande grandissante.

Il est aussi important de noter que le niveau de criticité est très différent d’un métal à l’autre - pour aller plus loin, voir cet [article du BRGM](http://www.mineralinfo.fr/page/matieres-premieres-critiques) - et que la contribution du numérique à l’épuisement d’un métal est très variable selon les métaux (c’est un “petit contributeur” pour des métaux communs comme le cuivre, mais un gros contributeur pour certains métaux rares).

À voir également : la figure 3-19 du livre “Les limites à la croissance” qui montre que l'impact environnemental augmente quand la concentration baisse.

JM Jancovici utilise cette image parlante : Lorsqu'on cherche les œufs de Pâques dans le jardin, on trouve en premier les œufs les plus gros et les moins bien cachés, et puis plus le temps passe plus on trouve les œufs les plus petits et les mieux cachés.
