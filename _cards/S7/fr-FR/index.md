---
title: Réduire le nombre et la taille des écrans
backDescription: >-
  A-t-on besoin de grands écrans publicitaires ? Ou encore d'un écran tactile
  sur notre machine à laver ? Les écrans sont partout et la taille moyenne des
  écrans TV a plus que doublé en 20 ans. Un écran plus petit nécessite moins de
  matières premières et d'énergie lors de sa fabrication. Il consomme aussi
  moins d'électricité à l’utilisation.
---

_Source : [étude Statista 2021](https://www.statista.com/statistics/961283/united-states-average-tv-screen-size/), chiffres USA, entre 1998 et 2018 la taille moyenne des écrans TV passe de 23 à 47 pouces._

Au-delà de l’inflation de leur taille, les écrans envahissent tous les espaces de nos vies, souvent sans apporter de vrai intérêt supplémentaire. Par exemple, un écran tactile sur l’autoradio d’une voiture ou sur un équipement électroménager aura nécessité bien plus de ressources à sa fabrication que l’équivalent non-digital, rendra l’équipement moins robuste sur le long terme (et donc réduira statistiquement sa durée de vie), le tout sans vraiment apporter de valeur ajoutée.

À noter aussi qu’un vidéoprojecteur consomme moins de ressources à la fabrication et moins d’énergie à l’utilisation que la plupart des télévisions.
