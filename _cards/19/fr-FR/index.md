---
title: Stress hydrique
backDescription: >-
  L'extraction minière consomme énormément d'eau douce. Pour les nombreux
  gisements situés dans des zones soumises à un stress hydrique, ces besoins en
  eau douce entrent en concurrence avec les besoins agricoles et les besoins
  quotidiens des populations locales.
---

Source : rapport de France Stratégie "La consommation de métaux du numérique : un secteur loin
d’être dématérialisé", 2020, p.24 à 26
A noter que le coût énergétique et environnemental très élevé de la désalinisation de l’eau,
envisagée pour alimenter certaines exploitations minières, n'en fait pas une solution durable.
Par exemple : l'extraction et raffinage du cuivre au Chili nécessite beaucoup d’eau douce dans un
endroit très sec (désert d’Atacama). Ceci nécessite de faire venir de l’eau de mer par tuyau, et donc
des usines de désalinisation (alimentées en électricité par des centrales à charbon...).
