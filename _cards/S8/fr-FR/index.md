---
title: Raisonner nos usages numériques
backDescription: >-
  Utiliser une qualité vidéo réduite, sortir des schémas de captation
  d’attention forcée type “autoplay”, et questionner un usage vidéo intensif -
  Privilégier un accès internet par câble ou wifi, éviter la 3G/4G/5G plus
  énergivore - Réduire la quantité de données stockées, et favoriser leur
  stockage local
---

Une fois que les infrastructures réseau, data centers et équipements sont construits et en place, les usages à proprement parler n’ont qu’un faible impact : l’impact vient très principalement de l’amortissement carbone du matériel qu’il a fallu fabriquer pour permettre l’usage.

Pour dire les choses simplement, ce n’est pas d’utiliser le tuyau qui compte, c'est de fabriquer le tuyau, une fois que le tuyau est en place, qu’il soit utilisé ou non ne change presque rien aux impacts générés.

Donc les empreintes en gramme par heure de vidéo, par email ou par toute autre usage sont à prendre avec des pincettes : dans ces calculs la grande majorité du CO2 émis mentionné vient du fait qu’il a fallu fabriquer du matériel pour permettre cet usage.

Les usages sont tout de même à prendre en compte dans la mesure où leur évolution conditionne le "dimensionnement des futurs tuyaux”, et dans les usages les mails sont insignifiants, le premier sujet est de loin la vidéo.

Pour calculer **l'empreinte carbone de la vidéo**, l'IEA a mis en place un simulateur d’empreinte carbone d’une heure de vidéo sur cette page. Voici également un tableau récap de l’impact d’1 heure de vidéo en fonction de différentes sources :

| 1h video | [Netflix](https://assets.ctfassets.net/4cd45et68cgf/64MBL9uJMEi3Otk6YBSDQG/7a3edca0600f522e84ed5d127c5f24d9/0220_US_EN_Netflix_EnvironmentalSocialGovernanceReport-2020-FINAL.pdf) (page 6) | Despacito (Bashroush) | Birdbox (Save on Energy) | TSP (corrected) | IEA updated | Australia | United Kingdom | France |
| -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ------------------------ | --------------- | ----------- | --------- | -------------- | ------ |
| kWh      |                                                                                                                                                                                             |                       |                          | 0,756           | 0,0769      |
| g CO2e   | <100                                                                                                                                                                                        | 768                   | 400                      | 394             | 36          | 54        | 18             | 4      |

[Article et Simulatateur IEA](https://www.iea.org/commentaries/the-carbon-footprint-of-streaming-video-fact-checking-the-headlines) <br/> [Article Source Carbon Brief](https://www.carbonbrief.org/factcheck-what-is-the-carbon-footprint-of-streaming-video-on-netflix/)

L'empreinte carbone varie grandement en fonction de plusieurs paramètres :

- lieu où est situé l'utilisateur et le serveur (énergie primaire utilisée pour produire l’électricité)
- équipement utilisé pour le visionnage
- qualité d'image associée
- type d'encodage vidéo
- type de connexion au réseau (wifi ou cellulaire)

En comparatif, voici les estimés de **l’empreinte carbone d'un message** :
| gCO2e | Base carbone ADEME | [BBC](https://www.bbc.com/news/technology-55002423) | [TSP Lean ICT REN (2018)](https://theshiftproject.org/wp-content/uploads/2019/04/Lean-ICT-Materials-REN-2018-updated-in-April-2019.xlsx) | [GreenIt (FR 2010)](https://www.greenit.fr/2010/02/24/un-sms-c-est-combien-de-co2/) | [youmatter](https://youmatter.world/fr/ecologie-mail-sms-message-empreinte-carbone/#:~:text=L'impact%20environnemental%20d'un,ou%20d'un%20message%20instantan%C3%A9&text=Pour%20un%20mail%20moyen%20tout,l'empreinte%20carbone%20%C3%A0%200.014.) |
| ----| ---|----|---|--|--|
|1 mail | 4 (**) | << 1
|1 mail avec pièce jointe | 35 (**) | | Mini 0,03 / Maxi 0,7
| 1 SMS | |||0,00215 | 0,014 (\*)
| 1 tweet | 0,02

(\*) basé sur Tim Berners-Lee 2010, actualisé depuis à la baisse<br/>
(\*\*) Les données ADEME semblent particulièrement peu réalistes

- En ce qui concerne le mode d’accès à internet : Un accès internet par câble ou wifi est moins énergivore qu’un accès internet par réseau mobile (3G/4G). Mais pour une même technologie cellulaire, la consommation baisse progressivement (optimisation progressive), exemple 4G : [The Shift Project Model 1byte](https://theshiftproject.org/wp-content/uploads/2018/10/Lean-ICT-Materials-1byte-Model-2018.xlsx) (2019) : écart de 5,8 entre WiFi et cellulaire
- Un serveur est allumé et donc alimenté en permanence. Une donnée stockée sur un serveur est systématiquement dupliquée plusieurs fois pour la rendre plus rapidement accessible et limiter le risque de la perdre en cas d’incident. Le stockage local mobilise moins d'équipements et ces équipements n'ont pas besoin d'être sous tension en permanence.
- Ecosia et Lilo ne consomment pas moins de ressources, ce qui gagne le plus est de faire moins de requêtes, d'où l'intérêt des favoris
- les PJ sont dupliquées pour tous les destinataires, donc le lien de partage temporaire peut-être une bonne option
