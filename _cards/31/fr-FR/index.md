---
title: Santé mentale
backDescription: >-
  L'usage intensif du numérique, notamment du smartphone et des réseaux sociaux,
  peut causer anxiété, dépendances, troubles de la concentration, ou encore
  accentuer nos biaux cognitifs. L'enfant et l'adolescent sont particulièrement
  vulnérables.
---

_Source : étude “[Anxiété, dépression et addiction liées à la communication numérique](https://journals.openedition.org/rfsic/2910)” (Marie-Pierre Fourquet-Courbet et Didier Courbet, 2017)_

L’étude “Anxiété, dépression et addiction liées à la communication numérique” (Marie-Pierre Fourquet-Courbet et Didier Courbet, 2017) synthétise les récents travaux publiés dans des revues scientifiques à comité de lecture, elle cherche à mieux comprendre les affects négatifs liés aux usages intensifs des technologies de communication numérique : dépendance et « addiction » à Internet, symptômes dépressifs et anxiétés associés aux réseaux sociaux et à la pratique du multitâche médiatique, peur de « rater quelque chose » (FOMO), perception de signaux fantômes stressante et nomophobie liée au smartphone. Dans une perspective transdisciplinaire, l’article analyse les processus psychologiques et psychosociaux, effets et déterminants majeurs des phénomènes impliquant affects négatifs et usages intensifs de la communication numérique.

Plus de détails sur le sujet dans la partie “Focus sur les sujets clés ” de ce guide.
