---
title: Dé-numériser nos activités
backDescription: >-
  Le numérique engendre des effets rebonds et accélère nombre d’activités
  humaines. Numériser ne permet généralement pas de réduire les impacts
  environnementaux au total. Tout n’a pas besoin d’être numérique : on peut
  penser low-tech (simple et accessible), ou encore maintenir un accès humain et
  sans numérique aux services publics et d'entreprise.
---

Pour en apprendre plus sur la philosophie Low-tech, RDV par exemple sur [le site de l’association Low Tech Lab](https://lowtechlab.org/fr).

Si l’on repense aux meilleurs moments que l’on passe dans sa semaine ou dans sa vie, ceux-ci sont rarement sur son smartphone ou derrière un écran
