---
title: Consommation d’énergies fossiles
backDescription: ''
---

_Sources :_

- _Recto : rapport de GreenIT.fr "[Empreinte environnementale du numérique mondial](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf)", 2019_
- _Verso :_
  - _85% selon le rapport de BP "[Statistical Review - 2019](https://www.bp.com/content/dam/bp/business-sites/en/global/corporate/pdfs/energy-economics/statistical-review/bp-stats-review-2019-full-report.pdf)"_
  - _81% selon le rapport de l'Agence internationale de l'énergie "[Key World Energy Statistics 2020](https://www.iea.org/reports/key-world-energy-statistics-2020)"_

En France l’électricité est principalement de source nucléaire, donc certain·e·s participant·e·s ont du mal à faire le lien entre énergies fossiles et production d'électricité. Attention, même en France lorsque l’on prend en compte toute l’énergie consommée (et pas juste l'électricité), la première source d’énergie est le pétrole, la seconde est le gaz et le nucléaire n’arrive qu’en 3ème position.

Bien rappeler que le chiffre de 2/3 de l'électricité est produite à partir de charbon ou de gaz à niveau mondial, et que notre usage du numérique ne s'arrête pas aux frontières (la plupart des serveurs ne sont pas sur le territoire, les équipements sont fabriqués en Chine...)
