---
title: Obsolescence technique
backDescription: >-
  Il s'agit de la réduction de la durée de vue d'un bien par des facteurs
  techniques : (1) obsolescence matérielle : fragile, difficile à réparer,
  pièces détachées chères… (2) obsolescence logicielle : incompatibilités,
  ralentissements, durée limitée du support… L'obsolescence est programmée
  lorsque la réduction de la durée de vue est sciemment visée dès la conception
  du produit.
---

La durée de vie d'un ordinateur a été divisée par près de 3 depuis 1985, passant de 11 à 4 ans. La durée de vie estimée d'un smartphone est autour de 2 ans.

On parle souvent d'obsolescence technique ou bien d'obsolescence matérielle.

Point de MIR parle aussi "d'obsolescence par usage inapproprié" = méconnaissance du produit (matériel et logiciel) conduisant à cesser son utilisation plus vite.

En France, la notion juridique d’obsolescence programmée a été utilisée pour porter plainte contre des fabricants d'imprimante en 2017. Par ailleurs Apple a finalement payé une amende pour “défaut de communication” pour “l’affaire des batteries iPhone” début 2020.

Pour aller plus loin, voir la section “Obsolescences” dans la partie approfondissement de ce guide.
