---
title: Non utilisation du matériel encore fonctionnel
backDescription: >-
  Devenus inutilisés par les mécanismes de l'obsolescence technique et
  psychologique, beaucoup d'équipements encore fonctionnels restent stockés dans
  des tiroirs… avant souvent d'être jetés des années plus tard. On estime que
  100 millions de smartphones sont inutilisés dans les loyers français.
---

_Source données : rapport du Sénat "[100 millions de téléphones portables usagés](https://www.senat.fr/rap/r15-850/r15-850_mono.html)”, 2016_

Il y a entre 54 et 113 millions de téléphones "dormants" en France selon le rapport Ademe “[Équipements électriques et électroniques, données 2018](https://www.ademe.fr/sites/default/files/assets/documents/registre-deee-donnees-2018-rapport.pdf)”, p. 37, repris dans un rapport du Sénat, pour plus de 2/3 encore fonctionnels (gardés essentiellement comme solution de rechange, pour soi ou pour ses proches). Le chiffre des autres équipements numériques "dormants" (ordinateurs, objets connectés...) est fort probablement élevé aussi, mais nous n'avons pas trouvé d'étude ou rapport abordant cela.
