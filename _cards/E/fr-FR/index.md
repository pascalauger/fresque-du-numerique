---
title: Se divertir
backDescription: >-
  Le numérique accélère les flux et permet d’optimiser l’industrie et les
  services : logistique, transport, conception assistée, contrôles automatisés,
  marchés boursiers, gestion financière, archivage, services publics,
  sécurité...
---

Voir [carte A](/card/A).
